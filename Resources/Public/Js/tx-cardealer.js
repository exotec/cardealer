

// initial stuff
jQuery(function() {
    setCheckboxSelectLabel();
    initFlexslider();

    jQuery('.cardealer-slick').slick({
        dots: true,
        infinite: true,
        variableWidth: true,
        centerMode: true
    });


    // setting intital history state on list plugin page
    if ( jQuery('.pageUrl').length > 0 ) {
        initHistoryUrl = jQuery('.pageUrl').attr('href');
        history.pushState({url: initHistoryUrl}, 'Default list state', initHistoryUrl);
        // jQuery('.reset').trigger('click');
    }

    // add the show html div after list
    if ( jQuery('.tx-cardealer-list').length > 0 ) {
        // add div only once if more slicks as one on the page found
        jQuery.each(jQuery('.tx-cardealer-list'), function (i) {
            if(i == 0) {
                jQuery(this).parent().after('<div class="tx-cardealer-show" />');
            }
        });
    }

    // setting intital history state on slick plugin page
    if ( jQuery('.cardealer-slick').length > 0 ) {
        history.pushState({url: location.href}, 'Default slick state', location.href);
    }

});


// click backlink in popup
jQuery(document).on("click", ".tx-cardealer-show .backlink", function(e){
    e.preventDefault();
    closeDetails();
    return false;
});


// change form quicksearch
jQuery(document).on("change", ".quicksearch .ajax-filter", function(e){
    var data = jQuery('.tx-cardealer-filter form').serialize();
    doAjax(data, 'filter', window.location);

});

// change form and start search
jQuery(document).on("change", ".default-search .ajax-filter", function(e){
    var data = jQuery('.tx-cardealer-filter form').serialize();
    var filterUrl = jQuery('.filterUrl').attr('href');
    var url = jQuery('.pageUrl').attr('href');
    // console.log(filterUrl);
    // console.log(url);
    doAjax(data, 'list', filterUrl);
    doAjax(data, 'filter', filterUrl);
});

// checkbox toggle and button label
jQuery(document).on("change", ".tx-cardealer-filter .input", function(){
    toggleCheckedAll(this);
    setCheckboxSelectLabel();
});





// toggle: for + and -
jQuery(document).on("click", ".toggleNext span", function(){
    if( jQuery(this).hasClass('open') ) {
        jQuery(this).hide();
        jQuery(this).next('span.close').show();
    } else {
        jQuery(this).addClass('close');
        jQuery(this).hide();
        jQuery(this).prev('span.open').show();
    }
    jQuery(this).closest('.item').find('.toggleContent').slideToggle();
});

// toggle: filter button in responsive view
jQuery(document).on("click", ".toggle-filter", function(e){
    e.preventDefault();
    jQuery('.tx-cardealer-filter form').toggleClass('d-none','');
});

// toggle: features checkboxes
jQuery(document).on("click", ".moreOptions", function(e){
    e.preventDefault();
    jQuery(this).next('.toggleContent').slideToggle('fast');
});

// toggle: dropdowns in searchform
jQuery(document).on("click", ".default-search .toggle-next", function(e){
    e.preventDefault();
    jQuery(this).next('.dropdown').slideToggle('fast');
});

jQuery(document).on("click", ".quicksearch .toggle-next", function(e){
    e.preventDefault();
    var menu = jQuery(".tx-cardealer-filter .dropdown");
    if( jQuery(this).hasClass('open') ) {
        jQuery('.toggle-next').removeClass('open');
        menu.hide();
    } else {
        jQuery('.toggle-next').removeClass('open');
        jQuery(this).addClass('open');
        jQuery(this).next('.dropdown').slideDown();
    }
});

// close dropdown if click somewhere outside
jQuery(document).on("mouseup", function(e){
    var menu = jQuery(".quicksearch .dropdown");
    if (!menu.is(e.target) // if the target of the click isn't the container...
        && menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
        menu.slideUp(300);
    }
});







// scroll to top clicking the bottom paginator
jQuery(document).on("click", ".paginator-bottom a", function(e){
    jQuery('body,html').animate({
        scrollTop: jQuery('.tx-cardealer-list').offset().top-170
    }, 500);
    return false;
});





jQuery(document).on("click", ".ajaxLink", function(e){
    var url = jQuery( this ).attr('href');
    var urlHistory = jQuery('.filterUrl').attr('href');
    // set history state
    if ( jQuery(this).hasClass('history') ) {
        e.preventDefault();
        // alert(urlHistory);
        history.pushState({url:url}, null, url);
        // alert(history.state.url);
    }

    if(jQuery(this).hasClass('paging') && jQuery(this).hasClass('current')) {
        return;
    }

    if( jQuery(this).hasClass('paging') ) {
        e.preventDefault();
        getData(url, 'list');
    }

    if( jQuery(this).hasClass("reset") ) {
        e.preventDefault();
        filterUrl = jQuery('.filterUrl').attr('href');
        getData(filterUrl, 'filter');
        getData(url, 'list');
    }

    if( jQuery(this).hasClass('show') ) {
        e.preventDefault();
        getData(url, 'show');
    }

});






function setCheckboxSelectLabel() {
    var wrappers = jQuery('.wrapper');
    jQuery.each( wrappers, function( key, wrapper ) {
        var checkboxes = jQuery(wrapper).find('.input');
        var property = jQuery(wrapper).attr('id');
        var prevText = '';
        var numberOfChecked = jQuery(wrapper).find('input.val:checkbox:checked').length;
        jQuery.each( checkboxes, function( i, checkbox ) {
            var button = jQuery(wrapper).find('.button-label');
            if( jQuery(checkbox).prop('checked') == true) {
                var text = jQuery(checkbox).next().html();
                var btnText = prevText + text;
                if(numberOfChecked >= 3) {
                    btnText = numberOfChecked +' '+ property +' ausgewählt';
                }
                jQuery(button).text(btnText);
                prevText = btnText + ', ';
            }
        });
    });
}

function toggleCheckedAll(elem) {
    var apply = jQuery(elem).closest('.wrapper').find('.apply-selection');
    jQuery('.apply-selection').fadeIn('slow');

    var val = jQuery(elem).closest('.dropdown').find('.val');
    var all = jQuery(elem).closest('.dropdown').find('.all');
    var input = jQuery(elem).closest('.dropdown').find('.input');

    if(!jQuery(input).is(':checked')) {
        jQuery(all).prop('checked', true);
        return;
    }

    if( jQuery(elem).hasClass('all') ) {
        jQuery(val).prop('checked', false);
    } else {
        jQuery(all).prop('checked', false);
    }
}


function closeLayer(layer) {
    jQuery(layer).hide().removeClass('visible');
    jQuery('body').removeClass('no-scroll');
}


function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}



function closeDetails(setHistory) {
    if(setHistory === undefined) {
        setHistory = true;
    }
    jQuery('.tx-cardealer-show').html('').removeClass('visible');
    historyUrl = jQuery('.pageUrl').attr('href');
    if(historyUrl === undefined){
        historyUrl = "/";
    }
    if (setHistory) {
        history.pushState({url: historyUrl}, null, historyUrl);
    }
    jQuery('body', window.parent.document).removeClass('no-scroll');
}

function doAjax(data,action,url,method, dataType) {
    showLoading(action);

    // default values for stupid IE and older Safari Versions
    if(action === undefined){
        action = "list";
    }
    if(method === undefined){
        method = "POST";
    }
    if(dataType === undefined){
        dataType = "html";
    }

    // remove chash from url
    testUri = url+'';
    test = testUri.split('?');
    // console.log(test);
    $params = getAllUrlParams(url);
    var noCacheHashURL = '';
    var i = 0;
    jQuery.each($params , function( key, value) {
        if(key != 'chash') {
            if(i++ > 0) {
                sep = '&'
            } else {
                sep = '';
            }
            noCacheHashURL += sep+key+'='+value;

        }
    });
    url = url+'';
    beforeSlash = url.split('?');
    url = beforeSlash[0]+'?'+noCacheHashURL;
    url = url+"&type=4711&tx_cardealer_pi1[action]="+action;
    // url = url+"&type=4711";
    // console.log(url);

    request = jQuery.ajax({
        url: url,
        method: method,
        data: data,
        dataType: dataType
    });
    request.done(function(response) {
        clearconsole();
        // success stuff
        jQuery(".tx-cardealer-" + action + "").parent('.tx-cardealer').replaceWith(response);
        if(action == 'filter') {
            setTimeout(function () {
                setCheckboxSelectLabel();
            }, 500)
        }
        var elem = jQuery('.pageUrl');
        if(elem.length > 0) {
            var realUrl = elem.attr('href');
            history.pushState({url: realUrl}, null, realUrl);
        }
    });
    request.fail(function( jqXHR, textStatus ) {
        // error stuff
        ( "Ooops an error occured: " + textStatus );
    });

}


function getData(url, action) {

    showLoading(action);

    if (action == 'show') {
        // load the whole details page into div
        jQuery('.tx-cardealer-show').addClass('visible');
        jQuery('body').addClass('no-scroll');

        jQuery('.tx-cardealer-show').load(url, function() {
            clearconsole();
        });

    } else {

        if(action == 'list') {
            char = '?';
            // console.log(url, 'list');
        } else {
            // remove chash from url
            $params = getAllUrlParams(url);
            var noCacheHashURL = '';
            var i = 0;
            jQuery.each($params , function( key, value) {
                if(key != 'chash' && key != 'tx_cardealer_pi1%5bidentifier%5d') {
                    if(i++ > 0) {
                        sep = '&'
                    } else {
                        sep = '';
                    }
                    if ( key == 'tx_cardealer_pi1%5blimit%5d') {
                        // unset limit per page to default from settings.list_limit
                        value = 0;
                    }
                    noCacheHashURL += sep+key+'='+value;
                }
            });
            url = url+'';
            beforeSlash = url.split('?');
            url = beforeSlash[0]+'?'+noCacheHashURL;
            // console.log(url, 'filter');
            char = '&';
        }
        jQuery('.tx-cardealer-'+action).load(url+char+'type=4711', function () {
            clearconsole();
            hideLoading(action);
        });
    }

}

function clearconsole()
{
    console.API;

    if (typeof console._commandLineAPI !== 'undefined') {
        console.API = console._commandLineAPI; //chrome
    } else if (typeof console._inspectorCommandLineAPI !== 'undefined') {
        console.API = console._inspectorCommandLineAPI; //Safari
    } else if (typeof console.clear !== 'undefined') {
        console.API = console;
    }

    console.API.clear();
}

function showLoading(action) {
    if(action == 'show') {
        return false;
    }
    jQuery(".tx-cardealer-"+action).find("input, select, button").prop("disabled", true);
    jQuery(".tx-cardealer-"+action).addClass('dimmed');
}

function hideLoading(action) {
    if(action == 'show') {
        return false;
    }
    jQuery(".tx-cardealer-"+action).find("input, select, button").prop("disabled", false);
    jQuery(".tx-cardealer-"+action).removeClass('dimmed');
}

function initFlexslider() {
    // // The slider being synced must be initialized first
    jQuery("#carousel").flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: true,
        itemWidth: 100,
        itemMargin: 5,
        asNavFor: "#slider",
        smoothHeight: true
    });
    jQuery("#slider").flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: true,
        sync: "#carousel",
        smoothHeight: true
    });
    setTimeout(function(){
        jQuery('.flexslider .flex-next').trigger('click');
        jQuery('.flexslider .flex-prev').trigger('click');
    },1000);
}


function getAllUrlParams(url) {

    url = url+'';

    // get query string from url (optional) or window
    var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

    // we'll store the parameters here
    var obj = {};

    // if query string exists
    if (queryString) {

        // stuff after # is not part of query string, so get rid of it
        queryString = queryString.split('#')[0];

        // split our query string into its component parts
        var arr = queryString.split('&');

        for (var i=0; i<arr.length; i++) {
            // separate the keys and the values
            var a = arr[i].split('=');

            // in case params look like: list[]=thing1&list[]=thing2
            var paramNum = undefined;
            var paramName = a[0].replace(/\[\d*\]/, function(v) {
                paramNum = v.slice(1,-1);
                return '';
            });

            // set parameter value (use 'true' if empty)
            var paramValue = typeof(a[1])==='undefined' ? true : a[1];

            // (optional) keep case consistent
            paramName = paramName.toLowerCase();
            paramValue = paramValue.toLowerCase();

            // if parameter name already exists
            if (obj[paramName]) {
                // convert value to array (if still string)
                if (typeof obj[paramName] === 'string') {
                    obj[paramName] = [obj[paramName]];
                }
                // if no array index number specified...
                if (typeof paramNum === 'undefined') {
                    // put the value on the end of the array
                    obj[paramName].push(paramValue);
                }
                // if array index number specified...
                else {
                    // put the value at that index number
                    obj[paramName][paramNum] = paramValue;
                }
            }
            // if param name doesn't exist yet, set it
            else {
                obj[paramName] = paramValue;
            }
        }
    }

    return obj;
}

function triggerAjaxClick(elem) {
    var speakingUrl = jQuery(elem).attr('href');
    var ajaxLink = jQuery(elem).prev('a.ajaxLink').attr('href');
    history.pushState({url:ajaxLink}, null, speakingUrl);
    jQuery(elem).prev('a.ajaxLink').trigger('click');
}



// back- and forward browser tab
window.onpopstate = function(event) {
    if(event.state !== null) {
        if(event.state.url !== undefined) {
            var url = event.state.url;
            var pathArray = url.split( '/' );
            // console.log(pathArray);

            if(inArray('details', pathArray)) {
                getData(url, 'show');

            } else  {
                getData(url, 'list');
                closeDetails(0);
            }
        }
    }
};

function jumpTo(h){
    var top = document.getElementById(h).offsetTop; //Getting Y of target element
    window.scrollTo(0, top);                        //Go there directly or some transition
}

==================================================
Cardealer - mobile.de API
==================================================

.. image:: Resources/Public/Img/logo.png?raw=true
   :alt: EXT:cardealer

Demo
~~~~~~~
`Open the demo`__

__ https://p487707.mittwaldserver.info/v9/public/


Site configuration
~~~~~~~

  CardealerPlugin:
    type: Extbase
    extension: Cardealer
    plugin: Pi1
    routes:
      -
        routePath: '/{makePlaceholder}/{slugPlaceholder}/{identifierPlaceholder}'
        _controller: 'Standard::show'
        _arguments:
          slugPlaceholder: car
          makePlaceholder: make
          identifierPlaceholder: identifier
      -
        routePath: '/page/{pagePlaceholder}/{sortByPlaceholder}/{sortOrderPlaceholder}/{limitPlaceholder}/{identifierPlaceholder}'
        _controller: 'Standard::list'
        _arguments:
          pagePlaceholder: page
          sortByPlaceholder: sortBy
          sortOrderPlaceholder: sortOrder
          limitPlaceholder: limit
          identifierPlaceholder: identifier
    defaultController: 'Standard::show'
    aspects:
      pagePlaceholder:
        type: StaticRangeMapper
        start: '1'
        end: '100'
      sortByPlaceholder:
        type: StaticValueMapper
        map:
          price: price
          mileage: mileage
          firstRegistration: first_registration
      sortOrderPlaceholder:
        type: StaticValueMapper
        map:
          asc: ASC
          desc: DESC
      limitPlaceholder:
        type: LimitValueMapper
      identifierPlaceholder:
        type: IdentifierValueMapper
      slugPlaceholder:
        type: PersistedAliasMapper
        tableName: tx_cardealer_domain_model_car
        routeFieldName: slug
      makePlaceholder:
        type: PersistedAliasMapper
        tableName: tx_cardealer_domain_model_make
        routeFieldName: slug
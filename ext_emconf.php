<?php
/**
 * Copyright (c) 2018. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

/***************************************************************
 * Extension Manager/Repository config file for ext: "cardealer"
 ***************************************************************/
$EM_CONF['cardealer'] = [
    'title' => 'Cardealer',
    'description' => 'TYPO3 mobile.de API including import tasks for multiple locations.',
    'category' => 'plugin',
    'author' => 'Alexander Weber',
    'author_email' => 'weber@exotec.de',
    'author_company' => 'www.exotec.de | Web-based business applications',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '11.5.0',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
